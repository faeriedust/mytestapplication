
// MyTestApplicationDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CMyTestApplicationDlg dialog
class CMyTestApplicationDlg : public CDialogEx
{
// Construction
public:
	CMyTestApplicationDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MYTESTAPPLICATION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void AddNumbers();
  CEdit MyNumber1Edit;
  afx_msg void OnEnChangeMynumber2();
  CEdit MyNumber2Edit;
  CEdit MySumEdit;
  CStatic MySumTextBox;
  CStatic MySumLabelControl;
};
